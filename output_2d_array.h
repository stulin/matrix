#ifndef OUTPUT_2D_ARRAY_H
#define OUTPUT_2D_ARRAY_H

void output_2d_array(double** arr, unsigned int rows, unsigned int columns);

#endif //OUTPUT_2D_ARRAY_H
