#include <stdio.h>

#include "create_matrix.h"
#include "transpose_quad_matrix.h"
#include "output_2d_array.h"

int main() {
    int n;
    printf("Enter the size of the square matrix: ");
    scanf("%d", &n);
    double** matrix = (double**)create_matrix(n, n);

    printf("Filling the matrix...\n");
    int i, j;
    for (i = 0; i < n; i++) {
        for (j = 0; j < n; j++) {
            printf("matrix[%d][%d] = ", i, j);
            scanf("%lf", &matrix[i][j]);
        }
    }

    printf("Entered matrix:\n");
    output_2d_array(matrix, n, n);

    transpose_quad_matrix(matrix, n);

    printf("Transpose matrix:\n");
    output_2d_array(matrix, n, n);

    delete_matrix(matrix, n);

}
