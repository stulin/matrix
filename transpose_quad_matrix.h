#ifndef TRANSPOSE_QUAD_MATRIX_H
#define TRANSPOSE_QUAD_MATRIX_H

void transpose_quad_matrix(double** arr, int n);

#endif //TRANSPOSE_QUAD_MATRIX_H
