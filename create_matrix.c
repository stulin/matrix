#include <malloc.h>

double** create_matrix(int rows, int columns) {
    double** arr = (double**)malloc(rows*sizeof(double*));
    int i;
    for (i = 0; i < rows; i++) {
        arr[i] = (double*)malloc(columns*sizeof(double));
    }
    return arr;
}

void delete_matrix(double** arr, int rows) {
    int i; for (i = 0; i < rows; i++) {
        free(arr[i]); //освобождение памяти под строку
    }
    free(arr);
}
