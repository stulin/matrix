#ifndef CREATE_MATRIX_H
#define CREATE_MATRIX_H

double** create_matrix(int rows, int columns);
void delete_matrix(double** arr, int rows);

#endif //CREATE_MATRIX_H
