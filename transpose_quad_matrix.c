void transpose_quad_matrix(double** arr, int n) {
    int i, j;
    for (i = 0; i < n; i++) {
        for (j = i; j < n; j++) {
            double tmp = arr[i][j];
            arr[i][j] = arr[j][i];
            arr[j][i] = tmp;
        }
    }
}
