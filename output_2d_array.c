#include <stdio.h>
void output_2d_array(double** arr, unsigned int rows, unsigned int columns) {
    int i, j;
    for (i = 0; i < rows; i++) {
        for (j = 0; j < columns; j++) {
            printf("%.1lf ", arr[i][j]);
        }
        printf("\n");
    }
}
